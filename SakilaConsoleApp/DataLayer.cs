﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using SakilaConsoleApp.Models;

namespace SakilaConsoleApp
{
    internal class DataLayer
    {
        public static void ListActors(SakilaContext context)
        {

            var listActorsQuery =
                (from a in context.Actors select a);

            Console.WriteLine("-------------------");
            Console.WriteLine("List of All Actors:");
            Console.WriteLine("-------------------");

            if (listActorsQuery.Count() == 0)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("No records returned.");
                Console.WriteLine("------------------");
            }
            else
            {
                foreach (Actor a in listActorsQuery)
                {
                    Console.WriteLine("------------------");
                    Console.WriteLine(
                    $"{a.FirstName}  {a.LastName}");
                    Console.WriteLine("------------------");
                }
            }
        }

        public static void AddActor(SakilaContext context)
        {
            Console.WriteLine("Enter actor first name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter actor last name:");
            string lastName = Console.ReadLine();

            Actor actor = new Actor();
            actor.FirstName = firstName;
            actor.LastName = lastName;

            context.Actors.Add(actor);
            try {
                context.SaveChanges();
                Console.WriteLine("Actor successfully added");
            } catch (Exception ex)
            {
                Console.WriteLine("Something went wrong, see error below:");
                Console.WriteLine(ex.ToString());
            }            
        }
        public static void SearchByFirstName(SakilaContext context)
        {
            Console.WriteLine("Enter actor first name:");
            string firstName = Console.ReadLine();
            var firstNameQuery =
                    (from a in context.Actors
                     where a.FirstName == firstName
                     select a);
            if (firstNameQuery.Count() == 0)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("No records returned.");
                Console.WriteLine("------------------");
            }
            else {
                foreach (Actor a in firstNameQuery)
                {
                    Console.WriteLine("------------------");
                    Console.WriteLine(
                    $"{a.FirstName}  {a.LastName}");
                    Console.WriteLine("------------------");
                }
            }
        }

        public static void SearchByLastName(SakilaContext context)
        {
            Console.WriteLine("Enter actor last name:");
            string lastName = Console.ReadLine();
            var lastNameQuery =
                (from a in context.Actors
                 where a.LastName == lastName
                 select a);
            if (lastNameQuery.Count() == 0)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("No records returned.");
                Console.WriteLine("------------------");
            }
            else
            {
                foreach (Actor a in lastNameQuery)
                {
                    Console.WriteLine("------------------");
                    Console.WriteLine(
                    $"{a.FirstName}  {a.LastName}");
                    Console.WriteLine("------------------");
                }
            }
        }

        public static void SearchByFilmName(SakilaContext context)
        {
            Console.WriteLine("Enter a film title:");
            string title = Console.ReadLine().ToUpper();
            var filmActorsQuery =
                 from f in context.Films
                 where f.Title.Contains(title)
                 select f;

            if (filmActorsQuery == null || filmActorsQuery.Count() == 0)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("No records returned.");
                Console.WriteLine("------------------");
            }
            else
            {
                foreach (var fa in filmActorsQuery) {
                    foreach (FilmActor fa2 in fa.FilmActors) {
                        Actor actor = context.Actors.Find(fa2.ActorId);
                        if (actor != null)
                        {
                            Console.WriteLine($"Actor ID: {fa2.ActorId}");
                            Console.WriteLine($"{actor.FirstName} {actor.LastName}");
                        }





                        /*                        var query = from fa3 in context.FilmActors
                                                            where fa3.ActorId == fa2.ActorId
                                                            join a in context.Actors on fa3.ActorId equals a.ActorId
                                                            orderby a.LastName
                                                            select a;*/
                        /*                        Actor actor = new Actor();
                                                actor.ActorId = fa2.ActorId;
                                                actor.FirstName = fa2.Actor.FirstName;
                                                actor.LastName = fa2.Actor.FirstName;*/

                        /*                        Console.WriteLine($"{fa2.ActorId}");
                                                foreach (Actor a in query) {
                                                    Console.WriteLine($"{a.FirstName} {a.LastName}");
                                                }*/
                        /*     Console.WriteLine($"{fa2.Actor.FirstName} {fa2.Actor.LastName}");*/

                    }
                }
/*                foreach (FilmActor fa in filmActorsQuery)
                {
                    var actorsQuery = 
                        from a in context.Actors
                        where a.ActorId == fa.ActorId
                        select new { 
                            FirstName = a.FirstName,
                            LastName = a.LastName
                        };

                    foreach (var actor in actorsQuery)
                    {
                        Console.WriteLine("------------------");
                        Console.WriteLine(
                        $"{actor.FirstName}  {actor.LastName}");
                        Console.WriteLine("------------------");
                    }
                }*/
            }
        }
    }
}
