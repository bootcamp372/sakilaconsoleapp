﻿

namespace SakilaConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {         
            string command = "";

            while (command != "Q")
            {
                Console.WriteLine("--------------------------");
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1 - List all the actors");
                Console.WriteLine("2 - Add a new actor");
                Console.WriteLine("3 - Search actor by first name");
                Console.WriteLine("4 - Search actor by last name");
                Console.WriteLine("5 - Search actor by film name");
                Console.WriteLine("Q - Quit");
                Console.WriteLine();
                Console.WriteLine("Enter your command:");
                command = Console.ReadLine().ToUpper();
                Console.WriteLine("--------------------------");
                var context = new SakilaContext();

                switch (command)
                {
                    case "1":
                        DataLayer.ListActors(context);
                        break;
                    case "2":
                        DataLayer.AddActor(context);
                        break;
                    case "3":
                        DataLayer.SearchByFirstName(context);
                        break;
                    case "4":
                        DataLayer.SearchByLastName(context);
                        break;
                    case "5":
                        DataLayer.SearchByFilmName(context);
                        break;
                    case "Q":
                        return;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                };
            } 
        }
    }
}